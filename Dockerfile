ARG BASE_IMAGE=python:3.8-alpine
FROM $BASE_IMAGE
WORKDIR /app
COPY . .
RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN chmod 777 -R .

EXPOSE 6767 6379

ENTRYPOINT ["python", "-m", "src"]
