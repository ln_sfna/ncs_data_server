import unittest
from src.data import models


class TestModels(unittest.TestCase):
    def setUp(self) -> None:
        print('Set up!')

    def test_write_data(self):
        id = 1
        type = 'text'
        data = "this is test text"
        expected = models.make_200_response("Data was saved successfully!")
        response = models.write_data(id, type, data)
        self.assertEqual(response, expected, "Results didn't match")

    def test_delete_data(self):
        id = 1
        expected = models.make_200_response("1 items have been deleted successfully!")
        response = models.delete_data(id)
        self.assertEqual(response, expected, "Results didn't match")


if __name__ == '__main__':
    unittest.main()
