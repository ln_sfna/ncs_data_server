import json


def make_404_response(text):
    response = {"Status code": 404, "Details": text}
    return json.dumps(response)


def make_200_response(text):
    response = {"Status code": 200, "Details": text}
    return json.dumps(response)
