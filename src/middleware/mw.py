from src.auth.models import check_chat_id_logged, check_user_state_login

from fastapi import Request
from fastapi.responses import JSONResponse

from src.middleware.query_filter import RequestFilter
from src.models import make_404_response


request_filter = RequestFilter(sql_injection=True, xss=True, nosql_injection=True)


class MyMiddleware:
    async def __call__(self, request: Request, call_next):

        if not request_filter.inspect_request(request):
            if check_chat_id_logged(request):
                response = await call_next(request)
                return response
            else:
                if request.url.path == "/auth/login":
                    if check_user_state_login(request):
                        response = await call_next(request)
                        return response
                    else:
                        return JSONResponse(make_404_response("You are not registered"))
                elif request.url.path == "/auth/register":
                    response = await call_next(request)
                    return response
                else:
                    return JSONResponse(make_404_response("You are not logged in"))
        else:
            return JSONResponse(make_404_response("Malicious request"))
