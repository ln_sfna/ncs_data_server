import re
from urllib.parse import urlparse, parse_qs

from fastapi.requests import Request


class RequestFilter:
    def __init__(self,
                 sql_injection=False,
                 nosql_injection=False,
                 xss=False):
        self.sql_injection = sql_injection
        self.nosql_injection = nosql_injection
        self.xss = xss

    def inspect_request(self, request: Request):
        parsed_url = urlparse(str(request.url))
        params = parse_qs(parsed_url.query)

        for key, values in params.items():
            for value in values:
                if self.__inspect_parameter(value):
                    return True

        return False

    def __inspect_parameter(self, param: str):
        if self.sql_injection:
            if self.__find_sql_injection(param):
                return True

        if self.nosql_injection:
            if self.__find_nosql_injection(param):
                return True

        if self.xss:
            if self.__find_xss(param):
                return True

        return False

    def __find_sql_injection(self, query: str):
        query = query.lower()
        pattern = r"(\s*([\0\b\'\"\n\r\t\%\_\\]*\s*(((select\s*.+\s*from\s*.+)|(insert\s*.+\s*into\s*.+)|(" \
                  r"update\s*.+\s*set\s*.+)|(delete\s*.+\s*from\s*.+)|(drop\s*.+)|(truncate\s*.+)|(alter\s*.+)|(" \
                  r"exec\s*.+)|(\s*(all|any|not|and|between|in|like|or|some|contains|containsall|containskey)\s*.+[" \
                  r"\=\>\<=\!\~]+.+)|(let\s+.+[\=]\s*.*)|(begin\s*.*\s*end)|(\s*[\/\*]+\s*.*\s*[\*\/]+)|(\s*(" \
                  r"\-\-)\s*.*\s+)|(\s*(contains|containsall|containskey)\s+.*)))(\s*[\;]\s*)*)+)"

        result = re.findall(pattern, query)

        if result:
            return True

        return False

    def __find_nosql_injection(self, query: str):
        query = query.lower()
        pattern = r"(\$ne)|(\$gt)|(\$regex)|(\$where)|(\$eq)"

        result = re.findall(pattern, query)

        if result:
            return True

        return False

    def __find_xss(self, query: str):
        query = query.lower()
        pattern = r"(\b)(on\S+)(\s*)=|javascript|<(|\/|[^\/>][^>]+|\/[^>][^>]+)>"

        result = re.findall(pattern, query)

        if result:
            return True

        return False
