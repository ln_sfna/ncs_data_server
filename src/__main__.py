import uvicorn

from fastapi import FastAPI
from starlette.middleware.base import BaseHTTPMiddleware

from src.auth.auth_router import auth_router
from src.data.data_router import data_router
from src.config import config
from src.middleware.mw import MyMiddleware

app = FastAPI()

my_middleware = MyMiddleware()
app.add_middleware(BaseHTTPMiddleware, dispatch=my_middleware)

app.include_router(auth_router)
app.include_router(data_router)


if __name__ == "__main__":
    uvicorn.run("src.__main__:app",
                host=config.server_host,
                port=config.server_port,
                reload=True)
