from pymongo import MongoClient

from src.config import config

# redis_db = redis.StrictRedis(host=config.redis_host, port=config.redis_port, db=config.redis_db)
mongo_client = MongoClient(f"mongodb://{config.mongo_host}:{config.mongo_port}")

mongo_db = mongo_client[config.mongo_table]
auth_db = mongo_db[config.mongo_pass]
data_db = mongo_db[config.mongo_data]
