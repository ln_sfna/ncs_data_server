from typing import Optional

from fastapi import APIRouter
from fastapi.responses import JSONResponse

from src.auth.models import password_check
from src.init import auth_db
from src.models import make_404_response, make_200_response

auth_router = APIRouter(
    prefix="/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@auth_router.get("/register")
async def auth_manager(id: Optional[int] = None, login: Optional[str] = None, pswd: Optional[str] = None):
    if login is None or pswd is None:
        return JSONResponse(make_404_response("Enter username and password"))
    else:
        if auth_db.find_one({"login": login, "password": pswd}) is None:
            if auth_db.find_one({"login": login}) is not None:
                return JSONResponse(make_404_response("Such login already exists"))

            password_ok = password_check(pswd)
            if password_ok:
                x = auth_db.insert_one({"chat_id": id, "login": login, "password": pswd})
                if x.acknowledged:
                    return JSONResponse(make_200_response("You has been successfully registered"))
                else:
                    return JSONResponse(make_404_response("Something went wrong"))
            else:
                return JSONResponse(make_404_response("Password is not strong"))

        else:
            return JSONResponse(make_404_response("You are already registered, use login instead"))


@auth_router.get("/login")
async def auth_manager(id: Optional[int] = None, login: Optional[str] = None, pswd: Optional[str] = None):
    if login is None or pswd is None:
        return JSONResponse(make_404_response("Enter username and password"))
    else:
        if auth_db.find_one({"login": login, "password": pswd}) is not None:
            new_id = {"$set": {'chat_id': id}}
            x = auth_db.update_one({"login": login, "password": pswd}, new_id)

            if x.acknowledged:
                return JSONResponse(make_200_response("You has been successfully logged in"))
            else:
                return JSONResponse(make_404_response("Something went wrong"))

        else:
            return JSONResponse(make_404_response("You are not registered yet, use register instead"))


@auth_router.get("/logout")
async def auth_manager(id: Optional[int] = None):
    if id is None:
        return JSONResponse(make_404_response("id was not specified"))
    else:
        if auth_db.find_one({"chat_id": id}) is not None:
            new_id = {"$set": {'chat_id': -1}}
            x = auth_db.update_one({"chat_id": id}, new_id)

            if x.acknowledged:
                return JSONResponse(make_200_response("You has been successfully logged out"))
            else:
                return JSONResponse(make_404_response("Something went wrong"))

        else:
            return JSONResponse(make_404_response("You are not registered yet, use register instead"))
