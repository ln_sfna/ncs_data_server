import re
from urllib.parse import parse_qs, urlparse

from src.init import auth_db

from fastapi import Request


def check_chat_id_logged(request: Request):
    parsed_url = urlparse(str(request.url))
    chat_id = parse_qs(parsed_url.query)["id"][0]
    x = auth_db.find_one({'chat_id': chat_id})
    if x is not None:
        return True
    return False


def check_user_state_login(request: Request):
    parsed_url = urlparse(str(request.url))
    login = parse_qs(parsed_url.query)["login"][0]
    x = auth_db.find_one({'login': login})
    if x is not None:
        return True
    return False


def password_check(password):
    """
    Verify the strength of 'password'
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """

    # calculating the length
    length_error = len(password) < 8

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~" + r'"]', password) is None

    # overall result
    password_ok = not (length_error or digit_error or uppercase_error or lowercase_error or symbol_error)

    return password_ok
    # return {
    #     'password_ok': password_ok,
    #     'length_error': length_error,
    #     'digit_error': digit_error,
    #     'uppercase_error': uppercase_error,
    #     'lowercase_error': lowercase_error,
    #     'symbol_error': symbol_error,
    # }
