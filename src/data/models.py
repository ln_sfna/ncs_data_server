import json
from datetime import datetime

from src.init import auth_db, data_db
from src.models import make_200_response, make_404_response


def show_all_data(id: int):
    row = auth_db.find_one({"chat_id": id})
    if row is not None:
        cur = data_db.find({"login": row["login"]})
        res = list(cur)
        if len(res) > 0:
            ind = 0
            response = {}
            for i in res:
                data = {}
                for key, val in i.items():
                    if key != '_id' and key != "login":
                        data[key] = val
                response[ind] = data
                ind += 1
            return make_200_response(json.dumps(response))
        else:
            return make_404_response(f"Nothing was found")
    else:
        return make_404_response(f"You are not logged in")


def write_data(id, type, data):
    row = auth_db.find_one({"chat_id": id})
    if row is not None:
        x = data_db.insert_one({"login": row["login"], "time": datetime.now().strftime("%d.%m.%Y %H:%M:%S"),
                                "type": type, "data": data})
        if x.acknowledged:
            return make_200_response("Data was saved successfully!")
        else:
            return make_404_response(f"Something went wrong")
    else:
        return make_404_response(f"You are not logged in")


def delete_data(id):
    row = auth_db.find_one({"chat_id": id})
    if row is not None:
        x = data_db.delete_many({"login": row["login"]})
        if x.acknowledged and x.deleted_count > 0:
            return make_200_response(f"{x.deleted_count} items have been deleted successfully!")
        elif x.acknowledged and x.deleted_count == 0:
            return make_200_response(f"Nothing to delete")
    else:
        return make_404_response(f"You are not logged in")