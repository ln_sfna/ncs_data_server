from typing import Optional

from fastapi import APIRouter
from fastapi.responses import JSONResponse

from src.data.models import *
from src.models import make_404_response

data_router = APIRouter(
    prefix="/data",
    tags=["data"],
    responses={404: {"description": "Not found"}},
)


@data_router.get("/show-all-data")
async def logs_manager(id: Optional[int] = None):
    if id is None:
        return JSONResponse(make_404_response("You should specify id"))
    else:
        return JSONResponse(show_all_data(id))


@data_router.get("/write-data")
async def logs_manager(id: Optional[int] = None, type: Optional[str] = None, data: Optional[str] = None):
    if data is None or type is None:
        return JSONResponse(make_404_response("No data or data type"))
    else:
        return JSONResponse(write_data(id, type, data))


@data_router.get("/delete-all-data")
async def project_manager(id: Optional[int] = None):
    if id is None:
        return JSONResponse(make_404_response("You should specify id"))
    else:
        return JSONResponse(delete_data(id))
