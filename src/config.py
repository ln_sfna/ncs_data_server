import os


class Config:
    mongo_host = os.getenv("MONGO_HOST")
    mongo_port = os.getenv("MONGO_PORT")
    mongo_table = os.getenv("MONGO_TABLE")
    mongo_pass = os.getenv("MONGO_AUTH")
    mongo_data = os.getenv("MONGO_DATA")

    server_host = os.getenv("SERVER_HOST")
    server_port = int(os.getenv("SERVER_PORT"))


config = Config()
